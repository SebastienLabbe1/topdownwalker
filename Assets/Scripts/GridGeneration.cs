﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Tilemaps;

public class Chunk {
    public List<List<string>> tiles;
    public Chunk(int size) {
        tiles = new List<List<string>>(new List<string>[size * size]);
    }
}

public class GridGeneration : MonoBehaviour
{
    Dictionary<string, string> interactions = new Dictionary<string, string>() 
    {
        ["Grass Hand"] = "Dirt",
        ["Dirt Hand"] = "Field",
    };

    Dictionary<string, string> tileLayer = new Dictionary<string, string>() 
    {
        ["Water"] = "Collidable",
    };

    public TilesData tiles;
    public BlobScript player;
    public int chunkDistance, seed, chunkSize;
    [Range(0f, 1f)]
    public float perlinScale;
    Vector3Int centerChunk, activeTile;
    int totalTiles = 0;
    Dictionary<Vector3Int, Chunk> chunks = new Dictionary<Vector3Int, Chunk>();
    HashSet<Vector3Int> chunksLoaded = new HashSet<Vector3Int>();

    void Start() {
        if(chunkSize == 0) chunkSize = 1;
        centerChunk = ChunkOf(player.transform.position);
        UpdateChunks();
        UpdateHighlight(activeTile);
    }

    void FixedUpdate() {
        Vector3Int newCenterChunk = ChunkOf(player.transform.position);
        if(newCenterChunk != centerChunk) {
            centerChunk = newCenterChunk;
            UpdateChunks();
        }
        Vector3Int newActiveTile = GridPosition(player.transform.position) + player.GetIntDirection();
        if(newActiveTile != activeTile) {
            UpdateHighlight(newActiveTile);
        }
    }

    void UpdateHighlight(Vector3Int newActiveTile) {
        tiles.uiMap.SetTile(activeTile, null);
        tiles.uiMap.SetTile(newActiveTile, tiles.border);
        activeTile = newActiveTile;
    }

    void UpdateChunks() {
        LoadChunks();
        UnloadChunks();
    }

    Vector3Int ChunkOf(Vector3Int position) {
        if(position.x < 0) position.x += 1 - chunkSize;
        position.x /= chunkSize;
        if(position.y < 0) position.y += 1 - chunkSize;
        position.y /= chunkSize;
        return position;
    }

    Vector3Int ChunkOf(Vector3 positionF) {
        Vector3Int position = GridPosition(positionF);
        return ChunkOf(position);
    }

    void GenerateChunk(Vector3Int position) {
        Vector3Int gridPosition = position * chunkSize;
        for(int x = 0; x < chunkSize; x ++) {
            for(int y = 0; y < chunkSize; y ++) {
                Vector3Int tempPos = gridPosition + new Vector3Int(x, y, 0);
                int tileIndex = TileIndex(tempPos);
                PlaceTile((tileIndex > 0) ? "Grass" : "Water", tempPos);
            }
        }
    }

    void LoadChunkFrom(Vector3Int position) {
        Chunk chunk;
        chunks.TryGetValue(position, out chunk);
        if(chunk == null) return;
        List<string> addTiles = new List<string>();
        List<Vector3Int> addPositions = new List<Vector3Int>();
        Vector3Int gridPosition = position * chunkSize;
        for(int x = 0; x < chunkSize; x ++) {
            for(int y = 0; y < chunkSize; y ++) {
                List<string> posTiles = chunk.tiles[x * chunkSize + y];
                Vector3Int tempPos = gridPosition + new Vector3Int(x, y, 0);
                for(int i = 0; i < posTiles.Count; i ++) {
                    addTiles.Add(posTiles[i]);
                    addPositions.Add(tempPos);
                }
            }
        }
        PlaceTiles(addTiles, addPositions);
    }

    void LoadChunk(Vector3Int position) {
        if(chunks.ContainsKey(position)) LoadChunkFrom(position);
        else GenerateChunk(position);
        chunksLoaded.Add(position);
    }

    void UnloadChunk(Vector3Int position) {
        Chunk chunk = new Chunk(chunkSize);
        Vector3Int gridPosition = position * chunkSize;
        for(int x = 0; x < chunkSize; x ++) {
            for(int y = 0; y < chunkSize; y ++) {
                Vector3Int tempPos = gridPosition + new Vector3Int(x, y, 0);
                chunk.tiles[x * chunkSize + y] = GetTileNamesAndClear(tempPos);
            }
        }
        chunks[position] = chunk;
        //chunks.Add(position, chunk);
        chunksLoaded.Remove(position);
    }


    void PlaceTiles(List<string> addTiles, List<Vector3Int> addPositions) {
        for(int i = 0; i < addTiles.Count; i ++) {
            PlaceTile(addTiles[i], addPositions[i]);
        }
    }


    void LoadChunks() {
        for(int i = -chunkDistance; i < chunkDistance + 1; i ++)
        for(int j = -chunkDistance; j < chunkDistance + 1; j ++) {
            Vector3Int position = centerChunk + new Vector3Int(i, j, 0);
            if(!chunksLoaded.Contains(position)) 
                LoadChunk(position);
        }
    }

    void UnloadChunks() {
        HashSet<Vector3Int> tempChunksLoaded = new HashSet<Vector3Int>(chunksLoaded);
        foreach(Vector3Int position in tempChunksLoaded) {
            if(ManhattanDistance(position, centerChunk) > chunkDistance) 
                UnloadChunk(position);
        }
    }

    float PerlinNoise(Vector3Int position) {
        float z = Mathf.PerlinNoise(perlinScale * (float)position.x, perlinScale * (float)position.y);
        return z;
    }

    int TileIndex(Vector3Int position) {
        if(ManhattanDistance(position, Vector3Int.zero) < 10) return 1;
        float z = PerlinNoise(position);
        return (int)(Mathf.Clamp(z, 0f, 1f) * (2 - 0.001f));
    }


    void PlaceDefaultTile(Vector3Int position) {
        int tileIndex = TileIndex(position);
        PlaceTile((tileIndex > 0) ? "Grass" : "Water", position);
        totalTiles ++;
    }

    public int abs(int x) {
        return (x < 0) ? -x : x;
    }

    public int ManhattanDistance(Vector3Int a, Vector3Int b){
        return Mathf.Max(abs(a.x - b.x),abs(a.y - b.y));
    }

    Tilemap GetTilemap(string tileName) {
        string tilemapName;
        tileLayer.TryGetValue(tileName, out tilemapName);
        if(tilemapName == null) tilemapName = "Floor";
        Tilemap tilemap;
        tiles.tilemaps.TryGetValue(tilemapName, out tilemap);
        return tilemap;
    }
    
    void RemoveTile(string tileName, Vector3Int position) {
        Tilemap tilemap = GetTilemap(tileName);
        if(tilemap == null) return; 
        tilemap.SetTile(position, null);
    }

    void PlaceTile(string tileName, Vector3Int position) {
        Tile tile = null;
        tiles.tiles.TryGetValue(tileName, out tile);
        if(tile == null) return;
        Tilemap tilemap = GetTilemap(tileName);
        if(tilemap == null) return; 
        tilemap.SetTile(position, null);
        tilemap.SetTile(position, tile);
    }

    public void Interact(string toolName, Vector3Int position) {
        string tileName = GetTileName(position);
        if(tileName == null) return;
        string newTileName;
        interactions.TryGetValue(string.Format("{0} {1}", tileName, toolName), out newTileName);
        Debug.Log(newTileName);
        if(newTileName == null || newTileName == tileName) return;
        RemoveTile(tileName, position);
        PlaceTile(newTileName, position);
    }

    public string GetTileName(Vector3Int position) {
        TileBase tile = null;
        for(int i = tiles.tilemapList.Count - 1; i >= 0; i --) {
            tile = tiles.tilemapList[i].GetTile(position);
            if(tile != null) return tile.name;
        }
        return null;
    }

    public List<string> GetTileNamesAndClear(Vector3Int position) {
        List<string> names = new List<string>();
        TileBase tile;
        for(int i = tiles.tilemapList.Count - 1; i >= 0; i --) {
            tile = tiles.tilemapList[i].GetTile(position);
            if(tile != null) {
                names.Add(tile.name);
                tiles.tilemapList[i].SetTile(position, null);
            }
        }
        return names;
    }

    public List<string> GetTileNames(Vector3Int position) {
        List<string> names = new List<string>();
        TileBase tile;
        for(int i = tiles.tilemapList.Count - 1; i >= 0; i --) {
            tile = tiles.tilemapList[i].GetTile(position);
            if(tile != null) names.Add(tile.name);
        }
        return names;
    }

    public Vector3Int GridPosition(Vector3 position) {
        return tiles.uiMap.WorldToCell(position);
    }
}
