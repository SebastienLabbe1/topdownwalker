﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Tilemaps;

public class TilesData : MonoBehaviour
{
    public Dictionary<string, Tilemap> tilemaps = new Dictionary<string, Tilemap>();
    public Dictionary<string, Tile> tiles = new Dictionary<string, Tile>();

    public List<Tilemap> tilemapList;
    public List<Tile> tileList;

    public Tile border;
    public Tilemap uiMap;

    void Start() {
        for(int i = 0; i < tileList.Count; i ++) {
            tiles.Add(tileList[i].name, tileList[i]);
        }
        for(int i = 0; i < tilemapList.Count; i ++) {
            tilemaps.Add(tilemapList[i].name, tilemapList[i]);
        }
    }
}
