﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class BarScript : MonoBehaviour
{
    public Slider slider;
    public void SetBarFill(float fill) {
        slider.value = fill;

    }
}
