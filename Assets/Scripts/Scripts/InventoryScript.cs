﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;

public class InventoryScript : MonoBehaviour
{
    public int size;
    public InventoryUIScript inventoryUI;
    List<bool> slotEmpty;
    List<GameObject> items;
    List<bool> updated;
    int minEmpty = 0;
    [HideInInspector]
    int activeSlot = -1;

    void Start() {
        items = new List<GameObject>(new GameObject[size]);
        slotEmpty = new List<bool>(new bool[size]);
        for(int i = 0; i < size; i ++) slotEmpty[i] = true;
        updated = new List<bool>(new bool[size]);
        for(int i = 0; i < size; i ++) updated[i] = false;
        UpdateMinEmpty();
    }

    public void Add(GameObject obj) {
        if(IsFull()) return;
        if(!slotEmpty[minEmpty]) throw new InvalidOperationException("min empty is not empty");
        items[minEmpty] = obj;
        slotEmpty[minEmpty] = false; 
        updated[minEmpty] = true;
        UpdateMinEmpty();
    }
    
    public bool IsFull() {
        return minEmpty == -1;
    }

    void Clear(int i) {
        slotEmpty[i] = true;
        if(minEmpty == -1) minEmpty = i;
        else minEmpty = Mathf.Min(i, minEmpty);
        updated[i] = true;
        if(inventoryUI != null) inventoryUI.UpdateSlot(i);
    }

    public GameObject Remove(int i) {
        if(i >= size || slotEmpty[i]) return null;
        GameObject obj = items[i];
        Clear(i);
        if(inventoryUI != null) inventoryUI.UpdateSlot(i);
        return obj;
    }

    void UpdateMinEmpty() {
        if(minEmpty == -1) return;
        for(int i = minEmpty; i < size; i ++ ) {
            if(slotEmpty[i]) {
                minEmpty = i;
                return;
            }
        }
        minEmpty = -1;
    }

    public void Next() {
        activeSlot ++;
        if(inventoryUI != null) inventoryUI.UpdateSlot(activeSlot - 1);
        if(activeSlot >= size) activeSlot = -1;
        if(inventoryUI != null) inventoryUI.UpdateSlot(activeSlot);
    }

    public void Prev() {
        activeSlot --;
        if(inventoryUI != null) inventoryUI.UpdateSlot(activeSlot + 1);
        if(activeSlot <= -2) activeSlot = size - 1;
        if(inventoryUI != null) inventoryUI.UpdateSlot(activeSlot);
    }

    public string GetToolName() {
        if(activeSlot == -1) return "Hand";
        return "";
    }

    public int GetActiveSlot() {
        return activeSlot;
    }
}
