﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class StandScript : MonoBehaviour
{
    public int price;
    public CollectableStats stats;

    void Start() {
        stats.gold = 0;
    }

    void OnTriggerEnter2D(Collider2D collider) {
        if (collider.gameObject.tag == "Player") {
            PlayerScript player = collider.GetComponent<PlayerScript>();
            if(player.TakeGold(price)) {
                player.Collect(stats);
            }
        }
    }
}
