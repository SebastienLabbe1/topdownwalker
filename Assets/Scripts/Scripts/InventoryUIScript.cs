﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class InventoryUIScript : MonoBehaviour
{
    public InventoryScript inventory;
    public GameObject slot;
    public GameObject hand;
    public float slotWidth;
    List<GameObject> slots;
    List<GameObject> objects;
    List<Image> borders;
    public Color activeColor, defaulfColor;

    void Start() {
        slotWidth = slot.transform.GetComponent<RectTransform>().rect.width;
        slots = new List<GameObject>(new GameObject[inventory.size+1]);
        objects = new List<GameObject>(new GameObject[inventory.size+1]);
        borders = new List<Image>(new Image[inventory.size+1]);
        for(int i = 0; i < slots.Count; i ++) {
            slots[i] = Instantiate(slot, transform);
            Vector3 offset = new Vector3(((float)i + 0.5f - (float)slots.Count/2f) * slotWidth, 0, 0);
            slots[i].transform.position = slots[i].transform.position + offset;
            borders[i] = slots[i].transform.GetChild(0).gameObject.GetComponent<Image>();
            UpdateBorder(i - 1);
        }
        Draw(hand, 0);
    }

    void Draw(GameObject obj, int index) {
        if(objects[index] != null) Destroy(objects[index]);
        objects[index] = Instantiate(hand, slots[index].transform);
        objects[index].transform.SetSiblingIndex(0);
    }

    public void UpdateSlot(int i) {
        UpdateBorder(i);
    }
    public void UpdateBorder(int i) {
        borders[i + 1].color = 
        (i == inventory.GetActiveSlot()) ? activeColor : defaulfColor;
    }
}
