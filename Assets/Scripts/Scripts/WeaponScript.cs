﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class WeaponScript : MonoBehaviour
{
    public SpriteRenderer sprite;
    public PolygonCollider2D collider2D;
    public BlobScript wielder;
    public float swingRange;
    public float swingSpeed;
    public float damage;
    bool isSwinging;
    float startAngle, endAngle;

    void Start() {
        Enable(false);
    }

    void FixedUpdate() {
        if(isSwinging) {
            float z = (gameObject.transform.rotation.eulerAngles.z + swingSpeed * Time.fixedDeltaTime)
                % 360;
            if(!inSwing(z)) {
                StopSwing();
            } else {
                transform.rotation = Quaternion.Euler(0, 0, z);
            }
        }
    }

    bool inSwing(float z) {
        if(endAngle > startAngle) {
            return z <= endAngle && z >= startAngle;
        } else {
            return z <= endAngle || z >= startAngle;
        }
    }

    void Enable(bool enabled) {
        isSwinging = enabled;
        sprite.enabled = enabled;
        collider2D.enabled = enabled;
    }

    void StopSwing() {
        Enable(false);
    }

    public void Swing(Vector3 direction) { 
        float z = 405 + Quaternion.LookRotation(Vector3.forward, direction).eulerAngles.z;
        startAngle = (z - swingRange) % 360;
        endAngle = (z + swingRange) % 360;
        transform.rotation = Quaternion.Euler(0, 0, startAngle);
        Enable(true);
    }

    void OnTriggerEnter2D(Collider2D collider) {
        if(collider.gameObject.tag == wielder.tag) return;
        BlobScript blob = collider.GetComponent<BlobScript>();
        if (blob != null) {
            blob.Damage(damage);
        }
    }

}
