﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class BlobScript : MonoBehaviour
{
    public Rigidbody2D rigidbody;
    public Animator animator;
    public BlobStats stats;
    public Slider healthSlider;
    public InventoryScript inventory;
    GridGeneration grid;
    Vector2 moveDirection;
    float health;

    void Start() {
        grid = GameObject.Find("Grid").GetComponent<GridGeneration>();
        health = stats.health;
        healthSlider.maxValue = health;
        healthSlider.value = health;
        moveDirection = new Vector2(0, -1);
    }

    void FixedUpdate() {
        if(rigidbody.velocity.sqrMagnitude > 0.01) {
            animator.SetFloat("Horizontal", rigidbody.velocity[0]);
            animator.SetFloat("Vertical", rigidbody.velocity[1]);
            animator.SetFloat("Speed", rigidbody.velocity.sqrMagnitude);
            moveDirection = rigidbody.velocity.normalized;
        }
    }
    
    public void MoveTowards(Vector2 direction) {
        rigidbody.velocity = direction * stats.speed;
    }

    public void Damage(float amount) {
        health -= amount;
        if(health <= 0) Die();
        UpdateHealthBar();
    }

    void Die() {
        Destroy(gameObject);
    }

    public void Heal(float amount) {
        health += amount;
        if(health > stats.health) health = stats.health;
        UpdateHealthBar();
    }

    public void Collect(CollectableStats stats) {
        Heal(stats.health);
    }

    public float getHealth() {
        return health;
    }

    public void Wack() {
        //string tileName = grid.GetTileID(transform.position, GetIntDirection());
        //Debug.Log(tileName);
    }

    public void Interact() {
        grid.Interact(inventory.GetToolName(), grid.GridPosition(transform.position) + GetIntDirection());
    }

    public void Attack() {
        //weapon.Swing(GetDirection());
    }

    public void UpdateHealthBar() {
        healthSlider.value = health;
    }

    public Vector3 GetDirection() {
        return moveDirection;
    }

    public Vector3Int GetIntDirection() {
        return new Vector3Int((int)Mathf.Round(moveDirection.x), (int)Mathf.Round(moveDirection.y), 0);
    }
}
