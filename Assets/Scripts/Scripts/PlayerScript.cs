﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;

public class PlayerScript : MonoBehaviour
{
    public PlayerStats stats;
    public BlobScript blob;
    public TextMeshProUGUI goldText;

    void Start() {
        UpdateGoldText();
    }

    void FixedUpdate() {
        UpdateGoldText();
    }

    void Update() {
        blob.MoveTowards(new Vector2(Input.GetAxisRaw("Horizontal"), Input.GetAxisRaw("Vertical")));
        if(Input.GetKeyDown("x")) blob.Wack();
        if(Input.GetKeyDown("z")) blob.Interact();
        if(Input.GetKeyDown("a")) blob.inventory.Prev();
        if(Input.GetKeyDown("s")) blob.inventory.Next();
    }
    public void Collect(CollectableStats colletable) {
        AddGold(colletable.gold);
        blob.Heal(colletable.health);
    }

    public void AddGold(int amount) {
        stats.gold += amount;
    }

    public bool TakeGold(int amount) {
        if(stats.gold < amount) return false;
        stats.gold -= amount;
        return true;
    }


    public void UpdateGoldText() {
        if(goldText != null) goldText.SetText("Gold : {0}", stats.gold);
    }
}
