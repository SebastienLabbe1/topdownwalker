﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CollectableScript : MonoBehaviour
{
    public CollectableStats stats;

    void OnTriggerEnter2D(Collider2D collider) {
        if (collider.gameObject.tag == "Player") {
            PlayerScript player = collider.GetComponent<PlayerScript>();
            player.Collect(stats);
            Destroy(gameObject);
        }
    }
}
