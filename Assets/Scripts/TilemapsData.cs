﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Tilemaps;

public class TilemapsData : MonoBehaviour
{
    public Dictionary<string, Tilemap> tilemaps = new Dictionary<string, Tilemap>();

    public List<Tilemap> tilemapList;

    void Start() {
        for(int i = 0; i < tilemapList.Count; i ++) {
            tilemaps.Add(tilemapList[i].name, tilemapList[i]);
        }
    }
}
