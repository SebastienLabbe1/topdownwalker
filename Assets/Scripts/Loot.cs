﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Loot : MonoBehaviour
{
    public List<GameObject> loots;
    bool isQuitting = false;

    void OnApplicationQuit()
    {
        isQuitting = true;
    }

    void OnDestroy() {
        if(isQuitting) return;
        for(int i = 0; i < loots.Count; i ++) {
            Vector3 direction = new Vector3(Random.value, Random.value, 0);
            Instantiate(loots[i], transform.position + direction, Quaternion.identity);
        }
    }
}
