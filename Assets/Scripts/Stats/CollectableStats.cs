﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CollectableStats : MonoBehaviour
{
    public int gold;
    public float health;
}
