﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BlobStats : MonoBehaviour
{
    public float health;
    public float defense;
    public float healthRegen;
    public float speed;
}
